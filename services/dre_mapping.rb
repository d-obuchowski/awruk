# encoding: UTF-8

class DreMapping

  HEADERS_NAMES = [
    "LOGIN","MATNR","NUMER","ARKTX","ILOSC","STATUS","CENA", "USERNAME","FVNR",
    "FVPOZ","NRSAP","OPIS","EAN13","EXTWG", "NUMER KLIENTA", "NUMER ZAMOWIENIA"
  ]

  def self.process(client_number, order_number, company_file, translations_file)
    new(client_number, order_number, company_file, translations_file).process
  end

  def initialize(client_number, order_number, company_file, translations_file)
    @client_number = client_number
    @order_number = order_number
    @company_file = company_file
    @translations_file = translations_file
  end

  def process
    parse_company_file
  end

  private

  attr_reader :client_number, :order_number, :company_file, :translations_file

  def translations
    @translations ||=
      begin
        spreadsheet = Roo::Spreadsheet.open(translations_file[:tempfile].path, extension: :xls)

        (2..spreadsheet.last_row).each_with_object({}) do |row, hash|
          hash[parse_cell_text(spreadsheet.cell(row, 'B')).gsub('/', '')] = parse_cell_text(spreadsheet.cell(row, 'C'))
        end
      end
  end

  def parse_cell_text(text)
    text = text.to_i if text.is_a? Float
    text.to_s.strip
  end

  def parse_company_file
    current_datetime = Time.now.strftime("%d-%m-%Y %H-%M-%S")

    result_file_name = "#{client_number}-DRE-#{current_datetime}.csv"
    company_file_path = company_file[:tempfile].path

    CSV.open(result_file_name, 'w:UTF-8', col_sep: ";", quote_char: '"', force_quotes: true, row_sep: "\r\n") do |csv_object|
      csv_object << HEADERS_NAMES

      CSV.foreach(company_file_path, col_sep: ";", headers: true, quote_char: ';').with_index do |row, index|
        result_values = []

        row.headers.each do |header|
          header_val = row[header]
          header_val = get_translation(row['"ARKTX"']) if header == '"OPIS"'
          result_values.append(remove_quotes(header_val))
        end

        result_values.append(client_number)
        result_values.append(order_number)
        csv_object << result_values
      end
    end

    result_file_name
  end

  def remove_quotes(value)
    return if value.nil?
    value.chomp('"').reverse.chomp('"').reverse
  end

  def get_translation(content)
    remove_quotes(content).split('/').map do |txt|
      (translations[txt].nil? || translations[txt].empty?) ? txt : translations[txt]
    end.join("/")
  end
end
