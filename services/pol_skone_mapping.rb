# encoding: UTF-8

class PolSkoneMapping
  def self.process(client_number, order_number, company_file, translations_file)
    new(client_number, order_number, company_file, translations_file).process
  end

  def initialize(client_number, order_number, company_file, translations_file)
    @client_number = client_number
    @order_number = order_number
    @company_file = company_file
    @translations_file = translations_file
  end

  def process
    parse_company_file
  end

  private

  attr_reader :client_number, :order_number, :company_file, :translations_file

  def generate_translations
    tranlations_file_path = translations_file[:tempfile].path
    texts_translations = {}
    spreadsheet = Roo::Spreadsheet.open(tranlations_file_path, extension: :xls)
    (2..spreadsheet.last_row).each do |row|
      name_property = spreadsheet.cell(row, 'D')
      name_values = spreadsheet.cell(row, 'E')
      texts_translations[name_property] = name_values
    end
    texts_translations
  end

  def parse_company_file
    current_datetime = Time.now.strftime("%d-%m-%Y %H-%M-%S")
    result_file_name = "#{client_number}-PolSkone-#{current_datetime}.xml"
    company_file_path = company_file[:tempfile].path
    translations = generate_translations
    File.open(company_file_path, 'r:UTF-8') do |company_file|
      doc = Nokogiri::XML(company_file)
      doc.at("Naglowek") << "<Numer_klienta>#{client_number}</Numer_klienta>"
      doc.at("Naglowek") << "<Numer_zamowienia>#{order_number}</Numer_zamowienia>"
      doc.xpath('//Pozycja').each do |node|
        describe_field_translation = get_translation(node.children[4].content, translations, ';')
        node.children[4].content = describe_field_translation
      end
      File.open(result_file_name, 'w:UTF-8', 0644) do |result_file|
        result_file.write "\uFEFF"
        result_file.write doc.to_xml
      end
    end
    result_file_name
  end

  def get_translation(content, translations, parse_char)
    results = []
    tab_content = content.split(parse_char)
    tab_content.each do |text|
      translation = translations.select{ |key, _| key == text }.values.first
      # translation ? (results << translation) : (results << text)
      results << translation
    end
    results.join('/')
  end
end
