# encoding: UTF-8

class WisniowskiMapping
  def self.process(client_number, order_number, company_file)
    new(client_number, order_number, company_file).process
  end

  def initialize(client_number, order_number, company_file)
    @client_number = client_number
    @order_number = order_number
    @company_file = company_file
  end

  def process
    parse_company_file
  end

  private

  attr_reader :client_number, :order_number, :company_file

  def parse_company_file
    current_datetime = Time.now.strftime("%d-%m-%Y %H-%M-%S")
    result_file_name = "#{client_number}-Wisniowski-#{current_datetime}.xml"
    company_file_path = company_file[:tempfile].path
    File.open(company_file_path, 'r:UTF-8') do |company_file|
      doc = Nokogiri::XML(company_file)
      doc.at("Order-Header") << "<Numer_klienta>#{client_number}</Numer_klienta>"
      doc.at("Order-Header") << "<Numer_zamowienia>#{order_number}</Numer_zamowienia>"
      File.open(result_file_name, 'w:UTF-8', 0644) do |result_file|
        result_file.write "\uFEFF"
        result_file.write doc.to_xml
      end
    end
    result_file_name
  end
end
