# encoding: UTF-8

class PortaMapping
  def self.process(*args)
    new(*args).process
  end

  def initialize(client_number, order_number, company_file, translations_file)
    @client_number = client_number
    @order_number = order_number
    @company_file = company_file
    @translations_file = translations_file
  end

  def process
    parse_company_file
  end

  private

  attr_reader :client_number, :order_number, :company_file, :translations_file

  def parse_company_file
    current_datetime = Time.now.strftime("%d-%m-%Y %H-%M-%S")
    result_file_name = "#{client_number}-Porta-#{current_datetime}.csv"
    company_file_path = company_file[:tempfile].path
    CSV.open(result_file_name, 'w:UTF-8', col_sep: ";", quote_char: '"', force_quotes: true, row_sep: "\r\n") do |csv_object|
      CSV.foreach(company_file_path, col_sep: ";", headers: false, quote_char: ';').with_index do |row, index|
        row.append(client_number)
        row.append(order_number)
        row_without_quotes = remove_quotes(row)
        row_without_quotes[1] = get_translation(row_without_quotes[1])
        csv_object << row_without_quotes
      end
    end
    result_file_name
  end

  def translations
    @translations ||=
      begin
        spreadsheet = Roo::Spreadsheet.open(translations_file[:tempfile].path, extension: :xls)

        (2..spreadsheet.last_row).each_with_object({}) do |row, hash|
          hash[parse_cell_text(spreadsheet.cell(row, 'B'))] = parse_cell_text(spreadsheet.cell(row, 'C'))
        end
      end
  end

  def parse_cell_text(text)
    text = text.to_i if text.is_a? Float
    text.to_s.strip
  end

  def get_translation(content)
    remove_quotes(content).split('.').map do |txt|
      (translations[txt].nil? || translations[txt].empty?) ? txt : translations[txt]
    end.join(".")
  end

  def remove_quotes(row)
    if row.is_a? Array
      row.map { |s| s.chomp('"').reverse.chomp('"').reverse }
    else
      row.chomp('"').reverse.chomp('"').reverse
    end
  end
end
