#!/usr/bin/env ruby
# encoding: UTF-8

require 'rubygems'
require 'sinatra'
require 'haml'
require 'pry'
require 'roo'
require 'roo-xls'
require 'nokogiri'
require 'csv'

require_relative 'services/dre_mapping'
require_relative 'services/pol_skone_mapping'
require_relative 'services/porta_mapping'
require_relative 'services/wisniowski_mapping'


Logger.class_eval { alias :write :'<<' }
$error_logger      = File.new( File.join(File.dirname(File.expand_path(__FILE__)), 'log', 'error.log'), 'a+' )
$error_logger.sync = true

before do
  env['rack.errors'] = $error_logger
end

COMPANIES_NAMES = {
  Dre: 'Dre',
  PolSkone: 'PolSkone',
  Porta: 'Porta',
  Wisniowski: 'Wisniowski'
}

get '/' do
  haml :index
end

post '/upload' do
  case params[:company_name]
  when COMPANIES_NAMES[:PolSkone]
    result_file_name = PolSkoneMapping.process(
      params[:client_number],
      params[:order_number],
      params[:company_file],
      params[:translations_file]
    )
  when COMPANIES_NAMES[:Dre]
    result_file_name = DreMapping.process(
      params[:client_number],
      params[:order_number],
      params[:company_file],
      params[:translations_file]
    )
  when COMPANIES_NAMES[:Porta]
    result_file_name = PortaMapping.process(
      params[:client_number],
      params[:order_number],
      params[:company_file],
      params[:translations_file]
    )
  else
    result_file_name = WisniowskiMapping.process(
      params[:client_number],
      params[:order_number],
      params[:company_file],
    )
  end
  attachment result_file_name
  content_type "application/octet-stream"
  result_file = File.open(result_file_name, 'r')
  content_file = result_file.read
  result_file.close
  File.delete(result_file_name)
  stream do |out|
    out << content_file
  end
end
