# frozen_string_literal: true

server 'awruk.2n.pl',
  roles:  %w(app web db),
  ssh_options: {
    user: 'awrukuser'
  }
set :deploy_to, "/home/awrukuser/awruk_app"
set :stage, :staging
set :branch, 'master'
